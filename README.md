isx47590131 -- Fedora 24



# DHCP FAILOVER

The target is to get a test system with two dhcp servers (primary and secondary) that are serving the same pool to a client.

## VIRTUAL ENVIRONTMENT

This practice will be implemented using a KVM virtualization system under Fedora (any other linux system can be used).

### INSTALL KVM (libvirtd, virt-manager)

First of all we should install virt-manager in our pc.

```

dnf -y install virt-manager

```


### CREATE AND INSTALL MAIN FEDORA QCOW2 TEMPLATE

To start a new VM we have to click on Create new virtual machine, then choose from where you
want to take the operating system (in my case PXE). OS type : Linux Version: latest
I used default ram and 2 CPU's. The storage memory 10 GiB.
CAUTION: select the right network or it will not work.

Before start VM we have to change somethings like boot options and network source.
Once the VM starts, a couple of things must be changed. Keyboard lenguage and software (fedora server)

### CREATE INCREMENTAL QCOW2 DISKS (dhcpd1, dhcpd2 and client)

Now we have to remove from the virt-manager the machine created. 
Then we go to /var/lib/libvirt/images/ and you have to see something like this:

```
drwx--x--x. 2 root root        4096 Oct 16 10:16 .
drwxr-xr-x. 8 root root        4096 Sep 15 09:54 ..
-rw-------. 1 qemu qemu 10739318784 Oct 13 11:58 fedora24.qcow2
```

Use order *qemu-img create -f qcow name_of_new_image -b name_of_old_image*

In my case: 

```
qemu-img create -f qcow dhcp_server1.qcow2 -b fedora24.qcow2 
qemu-img create -f qcow dhcp_client.qcow2 -b fedora24.qcow2 
qemu-img create -f qcow dhcp_server2.qcow2 -b fedora24.qcow2 

```

### CREATE SERVERS AND CLIENT KVM MACHINES

### CREATE AND CONFIGURE PRACTICE NETWORK ON VIRTUAL MACHINES

## DHCP SERVER 1

### NETWORK CONFIGURATION

### DHCPD SERVICE INSTALLATION

### DHCPD CONFIGURATION

## DHCP SERVER 2

### NETWORK CONFIGURATION

### DHCPD SERVICE INSTALLATION

### DHCPD CONFIGURATION

## DHCP CLIENT

### NETWORK CONFIGURATION

## TESTING ENVIRONTMENT


